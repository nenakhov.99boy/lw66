<?php

$numbers = ['1','11', '21', '6', '16'];
sort($numbers);
var_export($numbers);

function progression($numbers)
{
    $difference = $numbers[1] - $numbers[0];
    for ($i = 0; count($numbers)-1 > $i; $i++)
    {
        if ($numbers[$i + 1] - $numbers[$i] != $difference)
        {
            echo "\nNull\n";
            return;
        }
    }
    echo "\nThere is a progression! The difference is $difference\n";
}

progression($numbers);
$numbers = ['12', '2321', '312', '23131'];
var_export($numbers);
sort($numbers);
progression($numbers);
