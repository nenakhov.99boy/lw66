<?php
function comparison($first_string, $second_string)
{
    $first_string_letters = str_split($first_string);
    $second_string_letters = str_split($second_string);
    $amount = count($first_string_letters);
    $counter = 0;
    for ($i = 0; $i < $amount; $i++)
    {
        if (!isset($second_string_letters[$i]))
        {
            continue;
        }
        if ($first_string_letters[$i] === $second_string_letters[$i])
        {
            $counter++;
        }
    }
    echo "In this case there are $counter similar characters\n";
}

comparison('cinema', 'string');
comparison('theater', 'thread');




